import csv
import qa_checks.UtteranceQA as UtteranceQA
convo_level = ['','Contained', 'Abandoned', 'Transferred', 'Known data issue']
aban_reason = ['', 'Greeting', 'Intent']
cont_reason = ['', 'Full Containment', 'Partial Containment']
utter_type = ['', 'known intent', 'unknown intent', 'escalation', 'sentiment', 'designed_transfer']
esc_reason = ['', 'Greeting', 'Intent']

QA_combos = []

for convo in convo_level:
    for aban in aban_reason:
        for cont in cont_reason:
            for utter in utter_type:
                for esc in esc_reason:
                    test_combo = [convo, aban, cont, utter, esc]
                    class_qa_result = UtteranceQA.UtteranceQA().update_data(test_combo)
                    result = "OK" if class_qa_result else "Error"
                    QA_combos.append([convo+'|'+aban+'|'+cont+'|'+utter+'|'+esc, result])

with open('all_rules_QA.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    for combo in QA_combos:
        print(combo)
        writer.writerow(combo)

# print(len(QA_combos))