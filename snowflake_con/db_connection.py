# Config to pull data from Nucleus 
import platform
import sys 
import snowflake.connector
from snowflake.connector.pandas_tools import write_pandas
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.asymmetric import dsa
from cryptography.hazmat.backends import default_backend
import os 
import pandas as pd 
# examine the platform 
env_platform = platform.system() 
# set up the environment variables
userid = os.environ.get('ZID', '')
idrsa = os.environ.get('ID_RSA_P8', '')
try:
    password = os.getenv('ID_RSA_PASSCODE').encode()
except:
    password = None
role= os.environ.get('snowflake_prod_role', '')

class snowflake_connector: 
    def __init__(self, 
                    userid:str=userid, 
                    account: str='ally.us-east-1.privatelink',
                    warehouse:str='WH_TEAM_TECH_CONVOAI_ME', 
                    database:str='TEAM_TECH_CONVOAI_P', 
                    schema:str='CORE', 
                    role:str=role, 
                    private_key:str="",
                    cursor: str=""
                    ):
        '''
        feature_presentation_method: "binary", "counts", "TF-IDF"
        data: dataframe of training data 
        '''
         
        self.userid = os.getenv('ZID') #User must set envirment var
        self.account = account
        self.warehouse = warehouse
        self.database = database 
        self.schema = schema
        self.role = os.environ.get('snowflake_prod_role', '')
        self.private_key = private_key
        self.cursor = cursor 
    def create_cursor(self): 
        pwd = password
        pkey = idrsa.replace('|','\n').encode()
        
        
        p_key = serialization.load_pem_private_key(
            pkey
            ,password = pwd
            ,backend=default_backend()
        )
        pkb = p_key.private_bytes(
        encoding=serialization.Encoding.DER,
        format=serialization.PrivateFormat.PKCS8,
        encryption_algorithm=serialization.NoEncryption(),
        )
        
        self.private_key = pkb

        conn = snowflake.connector.connect(
            user = self.userid,
            account = self.account,
            warehouse = self.warehouse,
            database = self.database,
            schema = self.schema,
            role = self.role,
            private_key = pkb
        )
        #Snowflake data select and print
        self.cursor = conn.cursor()
    def query_data(self, query): 
        cur = self.cursor
        cur.execute(query)
        df = pd.DataFrame.from_records(iter(cur), columns=[x[0] for x in cur.description])
        return df 
    def create_table(self, query): 
        cur = self.cursor 
        cur.execute(query)
    def write_data(self, df:pd.DataFrame, table_name:str=""): 
        success, nchunks, nrows, _ = write_pandas(self.conn, df, table_name)
    def update_table(self, query): 
        cur = self.cursor
        cur.execute(query)
        df = pd.DataFrame.from_records(iter(cur), columns=[x[0] for x in cur.description])
        return df
    def insert_user(self, query):
        cur = self.cursor
        cur.execute(query)
        df = pd.DataFrame.from_records(iter(cur), columns=[x[0] for x in cur.description])
        return df
    def update_table_data(self, query, userid, account, warehouse, database, schema): 
      sc = snowflake_connector(userid=userid, 
                              account=account,
                              warehouse=warehouse, 
                              database=database, 
                              schema=schema)
      #sc.create_cursor(environment=environment, id_rsa_filename=id_rsa_filename, passcode_filename=passcode_filename, userid=userid) 
      sc.create_cursor()
      df = sc.update_table(query)
      df = pd.DataFrame.from_records(iter(cur), columns=[x[0] for x in cur.description])
      sc.cursor.close()
      return df
