from difflib import SequenceMatcher
import re 
import warnings
warnings.filterwarnings('ignore')
import nltk 
import pandas as pd

# remove PII
def remove_numbers(text):
    return re.sub(r'\w*\d\w*', '<NUMBER>', text).strip()

#TTA = Transfer to agent
#CL = Conversation Level
def check_trnfer_agent(TTA, CL ):
  
  xx =""
  if str(TTA).lower() == str("True").lower():
    xx = "Transferred"
  else:
    xx = CL

  return xx


#TTA = Transfer to agent
#ABN = Abandon Reason
def check_trnfer_agent_abandon(TTA, ABN ):
  
  xx =""
  if str(TTA).lower() == str("True").lower():
    xx = ""
  else:
    xx = ABN

  return xx

#TTA = Transfer to agent
#CL = Conversation Level
def check_remove_nan( CL ):
  
  xx =""
  if str(CL).lower() == str("nan").lower():
    xx = ""
  else:
    xx = CL

  return xx

#BR = Bot Response
def check_remove_buttons_text(bot_response_string):
    parsed_json = re.split("({)|,(?![ ])|(?<!https)(?<!ally):|(})", bot_response_string)
    parsed_json = [x for x in parsed_json if x != None]
    response_list = []
    curl_front = 0
    curl_back = 0
    for i, item in enumerate(parsed_json):
        if curl_front == curl_back and item != "{" and item != "":
            response_list.append(parsed_json[i])
        if item == "{":
            curl_front += 1
        elif item == "}":
            curl_back += 1
        elif item == "message":
            response_list.append(parsed_json[i+1])
        elif item == "button":
            response_list.append("button: " + parsed_json[i-4])
        elif item == "link":
            response_list.append("link: " + parsed_json[i+2])

    return response_list
