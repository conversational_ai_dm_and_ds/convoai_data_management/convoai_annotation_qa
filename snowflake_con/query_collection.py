import pandas as pd
# where case when lower(STRLEFT(x.conversation_id,3)) = 'cli' or lower(STRLEFT(x.conversation_id,4)) = 'test' THEN "Testing" else "Normal" end = "Normal"
def base_table_query(start_date, end_date=None): 
    query = f'''
    SELECT distinct
         a.conversation_turn_key,
        a.user_id as user_id_key,
        a.conversation_id,
        a.timestamp_of_user_text,
        CAST(a.timestamp_of_user_text as DATE) as timestamp_of_user_text_key,
        CAST(a.timestamp_of_user_text as TIME) as timestamp_of_user_text_tnz, 
        c.INTENT_NAME as bot_intent_name_key,
        a.turn_index,
        a.load_date,
        CAST(a.load_date as DATE) as LOAD_DATE_KEY,
        a.transferred_to_live_agent, 
        a.final_est_intent_flag, 
        a.est_response_accuracy, 
        a.est_conversation_level_flag, 
        a.est_conversation_level,
        lower(c.conversation_level) as conversation_level ,
        c.abandoned_reasons, 
        lower(c.transfer_reasons) as utterance_type, 
        lower(c.annotated_response_group) as annotated_response_group, 
        (c.transferred_to_live_agent) as trans_to_agent,
        Bot_record_type,
        lower(a.channel) as channel,
        annotator,
        case when TRIM(annotator) != '' then 'Annotated' else 'Not Annotated' end as annotator_data_label,
        1 as rec_count
FROM TEAM_TECH_CONVOAI_P.CORE.TBL_CHAT_LOG_D a
Left OUTER JOIN TEAM_TECH_CONVOAI_P.CORE.TBL_FEEDBACK_DATA_MASTER_RG c
ON (a.CONVERSATION_TURN_KEY = c.CONVERSATION_TURN_KEY)
Where 
    CAST(a.load_date as DATE) <= ( select max (CAST(x.load_date as DATE) ) from             TEAM_TECH_CONVOAI_P.CORE.TBL_FEEDBACK_DATA_MASTER_RG x)
    --and a.load_date between '2022-10-01' and '2022-10-09' 
    '''    
    query = query + '''    ORDER BY conversation_turn_key --limit 1000;'''
    return query

def query_report(): 
    query = f'''SELECT * FROM USER_SANDBOX_P.PZSW95."DAILY_REPORTING_DEPOSIT_TBL"
    
    '''
    return query
def create_dim_ddl(database, schema, table_name): 
    query = f'''create or replace TABLE {database}.{schema}.{table_name} (
	CONVERSATION_TURN_KEY VARCHAR(255),
	USER_ID_KEY VARCHAR(255),
	CONVERSATION_ID VARCHAR(255),
	LOAD_DATE TIMESTAMP_TZ(9),
	LOAD_DATE_KEY DATE,
	TURN_INDEX VARCHAR(12),
	BOT_INTENT_NAME_KEY VARCHAR(255),
	TIMESTAMP_OF_USER_TEXT TIMESTAMP_NTZ(9),
	TIMESTAMP_OF_USER_TEXT_KEY TIMESTAMP_NTZ(9),
	TIMESTAMP_OF_USER_TEXT_TNZ TIME(9),
	TRANSFERRED_TO_LIVE_AGENT BOOLEAN,
	EST_RESPONSE_ACCURACY VARCHAR(255),
	EST_CONVERSATION_LEVEL_FLAG VARCHAR(255),
	EST_CONVERSATION_LEVEL VARCHAR(255),
	CONVERSATION_LEVEL VARCHAR(255),
	UTTERANCE_TYPE VARCHAR(255),
	ANNOTATED_RESPONSE_GROUP VARCHAR(255),
	ANNOTATOR_DATA_LABEL VARCHAR(255),
	CNT_SUMMARY_CONTAINED_COVERAGE_GROUP VARCHAR(255),
	CNT_SUMMARY_COVERAGE_GROUP VARCHAR(255),
	CNT_SUMMARY_COVERAGE_IMPROPER_GROUP VARCHAR(255),
	BOT_RECORD_TYPE VARCHAR(255),
	CHANNEL VARCHAR(65));
    '''
    return query
def create_fact_ddl(database, schema, table_name): 
    query = f'''create or replace TABLE {database}.{schema}.{table_name} (
	CONVERSATION_TURN_KEY VARCHAR(255),
	BOT_INTENT_NAME_KEY VARCHAR(255),
	USER_ID_KEY VARCHAR(25),
	LOAD_DATE TIMESTAMP_TZ(9),
	LOAD_DATE_KEY DATE,
	TURN_INDEX VARCHAR(12),
	TIMESTAMP_OF_USER_TEXT TIMESTAMP_TZ(9),
	TIMESTAMP_OF_USER_TEXT_KEY TIMESTAMP_NTZ(9),
	TIMESTAMP_OF_USER_TEXT_TNZ TIME(9),
	CONVERSATIONS_COUNT NUMBER(38,0),
	ABANDONED_GREETINGS NUMBER(38,0),
	ABANDONED_KNOWN_UNKNOWN_INTENT NUMBER(38,0),
	ABANDONED_KNOWN_INTENT NUMBER(38,0),
	ABANDONED_UNKNOWN_INTENT NUMBER(38,0),
	CONTAINED NUMBER(38,0),
	ABANDONED NUMBER(38,0),
	ABANDONED_REASON_GREETING NUMBER(38,0),
	ABANDONED_REASON_INTENT NUMBER(38,0),
	CHANNEL_CHAT NUMBER(38,0),
	CHANNEL_MESSAGING NUMBER(38,0),
	TRANSFERRED NUMBER(38,0),
	CONTAINED_UNKNOWN_INTENT_FIRST_THEN_CONTAINED_AT_KNOWN_INTENT NUMBER(38,0),
	TRANSFERRED_ESCALATION_AT_GREETINGS NUMBER(38,0),
	TRANSFERRED_ESCALATION_WITH_KNOWN_AND_UNKNOWN_INTENTS NUMBER(38,0),
	TRANSFERRED_ESCALATION_WITH_KNOWN_INTENTS NUMBER(38,0),
	TRANSFERRED_ESCALATION_WITH_UNKNOWN_INTENTS NUMBER(38,0),
	TRANSFERRED_KNOWN_INTENT_AND_UNKNOWN_INTENT NUMBER(38,0),
	TRANSFERRED_ESCALATION NUMBER(38,0),
	TRANSFERRED_KNOWN_INTENT NUMBER(38,0),
	TRANSFERRED_UNKNOWN_INTENT NUMBER(38,0),
	TRANSFERRED_SENTIMENT NUMBER(38,0),
	TRANSFERRED_DESIGN_TRANSFER NUMBER(38,0),
	IMPROPER_RESPONSE NUMBER(38,0),
	IMPROPER_RESPONSE_COUNT NUMBER(38,0),
	PROPER_RESPONSE NUMBER(38,0),
	PROPER_RESPONSE_COUNT NUMBER(38,0),
	CUSTOMER_EXPERIENCE_METRIC NUMBER(38,0),
	CNT_OF_TRANSFERRED NUMBER(38,0),
	CNT_OF_ESC_GREET NUMBER(38,0),
	CNT_OF_ESC_OTHER NUMBER(38,0),
	CNT_OF_ABANDON_OTHER NUMBER(38,0),
	CNT_OF_ABANDON_GREETING NUMBER(38,0),
	CNT_OF_CONTAINED NUMBER(38,0),
	CNT_OF_KNOWN_INTENTS NUMBER(38,0),
	CNT_OF_KNOWN_AND_UNKNOWN_INTENTS NUMBER(38,0),
	CNT_OF_UNKNOWN_INTENTS NUMBER(38,0),
	CNT_OF_KNOWN_ESC_GREETINGS_INTENTS NUMBER(38,0),
	IF_A_RESPONSE NUMBER(38,0),
	CNT_OF_PROPER_RESPONSES NUMBER(38,0),
	CNT_OF_IMPROPER_TRANS_ALL NUMBER(38,0),
	CNT_OF_IMPROPER_TRANS_MISS NUMBER(38,0),
	IMPROPER_TRANSFER_POOR NUMBER(38,0),
	CNT_OF_IMPROPER_CONTAINED NUMBER(38,0),
	CNT_OF_IMPROPER_ABANDON NUMBER(38,0),
	CNT_SUMMARY_CONTAINED NUMBER(38,0),
	CNT_SUMMARY_COVERAGE NUMBER(38,0),
	RISK_ERROR_TYPE_I NUMBER(38,0),
	RISK_ERROR_TYPE_II NUMBER(38,0),
	RISK_ERROR_TYPE_III NUMBER(38,0),
	RISK_ERROR_TYPE_I_AND_II NUMBER(38,0),
	REC_COUNT NUMBER(38,0)
);
    '''
    return query
def truncate_tbl(database, schema, table_name): 
    query = f'''truncate TABLE {database}.{schema}.{table_name}; '''
    return query
def drop_tbl(database, schema, table_name): 
    query = f'''drop TABLE {database}.{schema}.{table_name}; '''
    return query
def insert_into_annotation_user(user_key, user_name, password):
    query = f'''insert into TEAM_TECH_CONVOAI_P.CORE.TBL_ANNOTATION_USER_LIST
    values (
    '{user_key}',
    '{user_name}',
    '{password}' ,
    CURRENT_DATE() ); '''
    return query
def update_annotation_user(user_key, user_name, password):
    query = f'''update TEAM_TECH_CONVOAI_P.CORE.TBL_ANNOTATION_USER_LIST
    set password = '{password}', 
        load_date = CURRENT_DATE()
    where key = '{user_key}'; '''
    return query
def delete_annotation_user(user_key):
    query = f'''delete from TEAM_TECH_CONVOAI_P.CORE.TBL_ANNOTATION_USER_LIST
        where key = '{user_key}'; '''
    return query
def select_annotation_user():
    query = f'''select * from TEAM_TECH_CONVOAI_P.CORE.TBL_ANNOTATION_USER_LIST; '''
    return query
