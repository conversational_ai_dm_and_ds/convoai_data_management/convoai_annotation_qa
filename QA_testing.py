import qa_checks.QualityAssurance as QA
import pandas as pd
import csv
# import qa_checks.UtteranceQA as UtteranceQA
# import pandas as pd
location_of_annotation_sheet = ""

file = pd.read_excel(location_of_annotation_sheet, sheet_name="data_metric_pull")
df = pd.DataFrame(file).fillna('')
business_intents = df["business_intent_name"]
conversation_ids = df[["conversation_id","Annotator"]].drop_duplicates().fillna('').values.tolist()
total = len(conversation_ids)
failed_conversations = []
qa_result = QA.QualityAssurance()
count = 0
for convo_id_annotator in conversation_ids:
    new_df = df.loc[df["conversation_id"] == convo_id_annotator[0]]
    new_df = new_df.loc[df["Annotator"] == convo_id_annotator[1]]
    annotators_df = df[["Annotator"]]
    qa_fields = new_df[["business_intent_name", "bot_intent_name", "conversation_level", "abandon_reason", "contained_reason", "Utterance_Type", "escalation_reason", "Annotator", "user_utterance", "bot_response"]].fillna('').values.tolist()
    qa_result.update_data(qa_fields)
    utterance = qa_result.get_valid_utterance()
    intent = qa_result.get_valid_intent()
    convo = qa_result.get_valid_convo()
    turn = qa_result.get_failed_turn()
    result = qa_result.get_qa_result()
    annotator = qa_result.get_annotator()
    if count % 1000 == 0:
        print(str(count)+"/"+str(total))
    if not result:
        failed_conversations.append([convo_id_annotator[0], result, utterance, intent, convo, turn, convo_id_annotator[1]])
    count += 1

with open("out.csv", "w", newline="") as f:
    writer = csv.writer(f)
    writer.writerow(["ConvoID", "Pass", "Utterance Level", "Intent", "Conversation Level", "Turn Index", "Annotator"])
    writer.writerows(failed_conversations)
print(len(failed_conversations))