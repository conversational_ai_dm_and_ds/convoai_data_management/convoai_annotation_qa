# convoai_annotation_qa

## Name

ConvoAI Annotation QA checks.

## Description

This code is run before the annotated conversations are uploaded to the master tables on snowflake. This provides additional checks that go beyond those available in an annotation tool. Future state will also manage QA conflicts for conversations that are unannotated.

## Usage

To run this, update the location_of_annotation_sheet variable in QA_testing for the location of the targeted annotation sheet. Run the file QA_testing. An output file "out.csv" will contain a record of all the conversations that have conflicts and what level they are found on. If no annotator id is provided, this represents an unannotated conversation.
