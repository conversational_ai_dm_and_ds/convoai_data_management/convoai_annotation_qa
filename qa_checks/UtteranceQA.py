#### CLASS TO EXAMINE ONE UTTERANCE AT A TIME ######
#### data in format:
#        [conversation_level, abandon_reason, contained_reason, utterance_type, escalation_reason]
import snowflake_con.db_connection as db_connection

class UtteranceQA:
    def __init__(self):
        self.data = None
        self.conversation_level = None
        self.abandon_reason = None
        self.contained_reason = None
        self.utterance_type = None
        self.escalation_reason = None
        self.user_utterance = None
        self.bot_response = None
        self.escalation_words = self.get_escalation_words()
        pass

    def get_escalation_words(self):
        sc = db_connection.snowflake_connector()
        query = f'''select word
            from TEAM_TECH_CONVOAI_P.CORE.TBL_DIM_RASA_NLU_LOOKUP
            where lookup = 'escalation_words';'''
        sc.create_cursor()
        df = sc.query_data(query)
        sc.cursor.close()
        return df.values.tolist()

    def check_conversation_level(self):
        if self.conversation_level == 'abandoned' or self.conversation_level == 'contained' or\
            self.conversation_level == 'transferred' or self.conversation_level == '' or\
            self.conversation_level == 'known data issue':
            return True
        else:
            return False

    def check_abandon_reason(self):
        if self.conversation_level == 'abandoned':
            if self.abandon_reason == 'greeting' or self.abandon_reason == 'intent':
                return True
        elif self.conversation_level == 'known data issue':
            return True
        else:
            if self.abandon_reason == '':
                return True
        return False

    def check_contained_reason(self):
        if self.conversation_level == 'contained':
            if self.contained_reason == 'full containment':
                return True
        elif self.conversation_level == 'abandoned' and self.abandon_reason == 'intent':
            if self.contained_reason == '' or self.contained_reason == 'partial containment':
                return True
        elif self.conversation_level == 'transferred':
            if self.contained_reason == '' or self.contained_reason == 'partial containment':
                return True
        elif self.conversation_level == 'known data issue':
            return True
        else:
            if self.contained_reason == '':
                return True
        return False

    def check_utterance_type(self):
        if self.conversation_level == 'abandoned':
            if self.abandon_reason == 'greeting':
                if self.utterance_type == 'known intent':
                    return True
            else:
                if self.utterance_type == 'known intent' or self.utterance_type == 'unknown intent' or self.utterance_type == 'escalation':
                    return True
        elif self.conversation_level == 'contained':
            if self.utterance_type == 'known intent':
                return True
        elif self.conversation_level == 'transferred':
            if self.utterance_type == 'known intent' or self.utterance_type == 'unknown intent' or\
                self.utterance_type == 'escalation' or self.utterance_type == 'sentiment':
                return True
            elif self.utterance_type == 'designed_transfer' and self.user_utterance[0] == '/':
                return True
        elif self.conversation_level == 'known data issue':
            return True
        else:
            if self.utterance_type == 'known intent' or self.utterance_type == 'unknown intent' or self.utterance_type == 'escalation':
                return True
        return False

    def check_escalation_reason(self):
        if self.utterance_type == 'escalation':
            if self.escalation_reason == 'greeting' or self.escalation_reason == 'intent':
                return True
        elif self.conversation_level == 'known data issue':
            return True
        else:
            if self.escalation_reason == '':
                return True
        return False
    
    def check_escalation_words(self):
        for word in self.escalation_words:
            word = word[0]
            if word in self.user_utterance:
                if self.conversation_level == 'Transferred':
                    if self.utterance_type == 'escalation':
                        return True
                    else:
                        return False
                else:
                    return False
        return True

    def update_qa_result(self):
        convo_level_qa = self.check_conversation_level()
        abandon_reason_qa = self.check_abandon_reason()
        contained_reason_qa = self.check_contained_reason()
        utterance_type_qa = self.check_utterance_type()
        escalation_reason_qa = self.check_escalation_reason()
        # escalation_word_qa = self.check_escalation_words()
        if convo_level_qa and abandon_reason_qa and contained_reason_qa and utterance_type_qa and escalation_reason_qa:
            return True
        return False
    
    def update_data(self, data):
        self.data = data
        self.conversation_level = data[0].lower()
        self.abandon_reason = data[1].lower()
        self.contained_reason = data[2].lower()
        self.utterance_type = data[3].lower()
        self.escalation_reason = data[4].lower()
        self.user_utterance = data[6].lower()
        self.bot_response = data[7].lower()
        return self.update_qa_result()
