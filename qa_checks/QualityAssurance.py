import qa_checks.ConvoQA as ConvoQA
import qa_checks.IntentQA as IntentQA
import qa_checks.UtteranceQA as UtteranceQA

#### CLASS TO EXAMINE ONE CONVO AT A TIME ######
#### data in format:
#    [[business_intent, rasa_intent, conversation_level, abandon_reason, contained_reason, utterance_type, escalation_reason, annotator],
#    [business_intent, rasa_intent, conversation_level, abandon_reason, contained_reason, utterance_type, escalation_reason, annotator],
#    [business_intent, rasa_intent, conversation_level, abandon_reason, contained_reason, utterance_type, escalation_reason, annotator]]
class QualityAssurance():
    def __init__(self):
        self.convo_data = [[]]
        self.valid_utterance = None
        self.valid_intent = None
        self.valid_convo = None
        self.failed_turn = None
        self.annotator = None
        self.valid = None
        self.convoqa = ConvoQA.ConvoQA()
        self.utteranceqa = UtteranceQA.UtteranceQA()
        self.intentqa = IntentQA.IntentQA()
        #self.update_qa_result()
        pass

    def get_qa_result(self):
        return self.valid

    def get_valid_utterance(self):
        return self.valid_utterance

    def get_valid_intent(self):
        return self.valid_intent

    def get_valid_convo(self):
        return self.valid_convo

    def get_failed_turn(self):
        return self.failed_turn
    
    def get_annotator(self):
        return self.annotator
    
    def update_data(self, data):
        self.convo_data = data
        self.failed_turn = None
        self.annotator = None
        self.update_qa_result()

    def update_qa_result(self):
        self.valid_convo = self.convoqa.update_data(self.convo_data)
        turn_cnt = 1
        annotator = None
        for utterance in self.convo_data:
            annotator = utterance[7]
            self.valid_intent = self.intentqa.update_data(utterance[0:2])
            self.valid_utterance = self.utteranceqa.update_data(utterance[2:])
            if not self.valid_intent or  not self.valid_utterance:
                self.failed_turn = turn_cnt
                break
            turn_cnt += 1
        self.annotator = annotator
        self.valid = self.valid_convo and self.valid_intent and self.valid_utterance
        # return self.valid_convo and self.valid_intent and self.valid_utterance